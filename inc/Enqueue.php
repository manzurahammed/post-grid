<?php

namespace TenUpPostGrid;

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

class Enqueue {
    public  function __construct(){
        add_action( 'wp_enqueue_scripts', [$this, 'load_assets'] );
    }
    
    /**
     * Load js/css for shortcode
     */
    public function load_assets() {
        wp_register_style( 'post-grid', TENUP_POST_GRID_URL . 'assets/css/post-grid.css', [], '1.0.0' );
        wp_register_script('post-grid',TENUP_POST_GRID_URL . 'assets/js/post-grid.js',['jquery']);
        wp_localize_script( 'post-grid', 'localize', [
            'ajaxurl' => admin_url( 'admin-ajax.php' )
        ] );
    }
}

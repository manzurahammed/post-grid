<?php

namespace TenUpPostGrid;

if ( !defined( 'ABSPATH' ) ) {
	exit;
}

class Helper {
    
    /**
     * @return array
     */
	public static function get_category_list() {

		$categories = get_terms( [
			'hide_empty' => true,
			'taxonomy'   => 'category',
			'exclude'    => 1,
		] );

		$cat_list = [];

		if ( is_wp_error( $categories ) ) {
			return $cat_list;
		} else {
			foreach ( $categories as $item ) {
				$cat_list[ $item->term_id ] = [ 'name' => $item->name, 'slug' => $item->slug, 'term_id' => $item->term_id ];
			}
		}
		return $cat_list;
	}
    
    /**
     * @param string $post_type
     * @param string $cat_id
     * @param int $per_page
     * @param int $offset
     * @return array
     */
	public static function get_post_by_category( $post_type = 'any', $cat_id = '', $per_page = 3, $offset = 0 ) {
		$args = [
			'post_type'      => $post_type,// post and custom post type
			'posts_per_page' => $per_page,
			'offset'         => $offset
		];
		if ( !empty( $cat_id ) ) {
			$args[ 'cat' ] = $cat_id;
		}
		$post_data = [];
		$query     = new \WP_Query( $args );
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				$post_data[ get_the_ID() ] = [
					'title'     => get_the_title(),
					'link'      => get_the_permalink(),
					'excerpt'   => wp_trim_words( strip_shortcodes( get_the_excerpt() ), 10 ),
					'image_url' => get_the_post_thumbnail_url( get_the_ID(), 'full' ),
					'cat_list'  => !empty( get_category( $cat_id )->slug ) ? get_category( $cat_id )->slug : '',
				];
			}
			wp_reset_postdata();
		}

		return $post_data;
	}
    
    /**
     * @param $post_list
     * @param false $init
     * @return string
     */
	public static function render_post_list( $post_list, $init = false ) {
		$markup = '';
		if ( !empty( $post_list ) ) {
			$active = $init?'active':'';
			foreach ( $post_list as $key => $item ) {
				$markup .= '<li class="post-grid-post-list '.$active.' ' . $item[ 'cat_list' ] . '">';
				$markup .= '<div class="post-grid-wrap">';
				if ( !empty( $item[ 'image_url' ] ) ) {
					$markup .= '<div class="post-grid-image-wrap">';
					$markup .= sprintf( '<img height="300" width="300" src="%s" class="post-grid-image">', esc_url( $item[ 'image_url' ] ) );
					$markup .= '</div>';
				}

				$markup .= '<div class="post-grid-details-wrap">';
				$markup .= sprintf( '<h2 class="post-grid-title"><a href="%s">%s</a></h2>', esc_attr( $item[ 'link' ] ), esc_html( $item[ 'title' ] ) );
				$markup .= sprintf( '<p class="post-grid-desc">%s</p>', $item[ 'excerpt' ] );
				$markup .= '</div>';
				$markup .= '</div>';
				$markup .= '</li>';
			}
		}
		return $markup;
	}
}

<?php

namespace TenUpPostGrid;

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

use TenUpPostGrid\Helper;

class Post_Grid_Builder {
    
    public function __construct() {
        add_shortcode( 'post_grid', [$this, 'post_grid_view'] );
        add_action( 'wp_ajax_get_post_list', [$this, 'get_post_list'] );
        add_action( 'wp_ajax_nopriv_get_post_list', [$this, 'get_post_list'] );
    }
    
    public function post_grid_view( $atts ) {
        $atts = shortcode_atts( [
            'post_per_page' => 3
        ], $atts, 'post_grid' );
        
        echo '<div class="post-grid-container">';
            if(!empty( current(Helper::get_category_list()))){
                echo $this->build_category_list( $atts[ 'post_per_page' ] );
                echo $this->build_post_list();
                echo $this->load_more_button();
            }else{
                printf('<h4 class="no-category-found">%s</h4>',__('No Post Found'));
            }
        echo '</div>';
        
        $this->load_script();
    }
    
    public function build_category_list() {
        $cat_list = Helper::get_category_list();
        $html = '';
        if ( !empty( $cat_list ) ) {
            $html = '<ul class="post-grid-cats-list">';
            $i = 0;
            foreach ( $cat_list as $key => $item ) {
                $active = ( $i == 0 ) ? "active" : "";
                $offset = ( $i == 0 ) ? 3 : 0;
                
                $html .= sprintf( '<li class="post-grid-cat"><a href="#" class="%s" data-slug="%s" data-offset="%s" data-cat-id="%d">%s</a></li>',
                    $active, $item[ 'slug' ], $offset, $key, $item[ 'name' ] );
                $i++;
            }
            $html .= '</ul>';
        }
        return $html;
    }
    
    public function build_post_list( $per_page = 3 ) {
        $cat = current( Helper::get_category_list() );
        if(!empty( $cat)){
            $post_list = Helper::get_post_by_category( 'post', $cat[ 'term_id' ], $per_page );
            return sprintf( '<ul class="post-grid-post-lists">%s</ul>', Helper::render_post_list( $post_list, true ) );
        }
    }
    
    public function get_post_list() {
        
        if ( empty( $_POST[ 'cat_id' ] ) ) {
            wp_send_json_error();
        }
        
        $cat_id = intval( $_POST[ 'cat_id' ] );
        $offset = intval( $_POST[ 'offset' ] );
        $post_list = Helper::get_post_by_category( 'any', $cat_id, 3, $offset );
        wp_send_json_success( [
            'response' => Helper::render_post_list( $post_list )
        ] );
    }
    
    public function load_more_button() {
        return sprintf( '<div class="post-grid-load-more"><button class="post-grid-load-more-button">%s</button></div>',
            __( 'Load More', 'post-grid' ) );
    }
    
    public function load_script() {
        wp_enqueue_style( 'post-grid' );
        wp_enqueue_script( 'post-grid' );
    }
}

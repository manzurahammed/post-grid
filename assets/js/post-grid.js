(function ($) {
    $(document).on('click', '.post-grid-cat a', function (e) {
        e.preventDefault();
        const $this = $(this),
            catID = $this.data('cat-id'),
            offset = parseInt($this.data('offset')),
            slug = $this.data('slug'),
			button = $(".post-grid-load-more-button");

        if (catID != '') {
			button.show()
        }else{
			button.hide()
		}

        if ($this.hasClass('active')) {
            return false;
        }

        $(".post-grid-cat > a").removeClass('active');
        $this.addClass('active');

        if (offset > 0) {
            post_grid_render(slug);
            return false;
        }
        var $data = {
            action: "get_post_list",
            cat_id: catID,
            offset: offset
        };

        $.ajax({
            url: localize.ajaxurl,
            type: "post",
            data: $data,
            success: function (response) {
                if (response.success) {
                    $(".post-grid-post-lists").append(response.data.response);
                }
                $this.data('offset', offset + 3)
                post_grid_render(slug);
            }
        })
    })

    $(document).on('click', '.post-grid-load-more-button', function (e) {
        e.preventDefault();
        const $this = $(this),
            activeCat = $(".post-grid-cat > a.active"),
            catID = activeCat.data('cat-id'),
            offset = parseInt(activeCat.data('offset')),
            slug = activeCat.data('slug');

        var $data = {
            action: "get_post_list",
            cat_id: catID,
            offset: offset
        };
        $.ajax({
            url: localize.ajaxurl,
            type: "post",
            data: $data,
            success: function (response) {
                if (response.success) {
                    if (response.data.response != '') {
                        $(".post-grid-post-lists").append(response.data.response);
                    } else {
                        activeCat.data('cat-id', '')
                        $this.hide("slow");
                    }
                }
                activeCat.data('offset', offset + 3)
                post_grid_render(slug);
            }
        })
    });

    const post_grid_render = (slug) => {
        $(".post-grid-post-list").removeClass('active');
        $(`.post-grid-post-list.${slug}`).addClass('active');
    }

})(jQuery);


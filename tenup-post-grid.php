<?php
/**
 * TenUp Post Grid Plugin
 *
 * @since             1.0.0
 * @package           Tenup_Post_Grid
 *
 * @wordpress-plugin
 * Plugin Name:       TenUp Post Grid
 * Plugin URI:
 * Description:       The best plugin for show post grid using shortcode
 * Version:           1.0.0
 * Author:            Manzur
 * Author URI:        https://profiles.wordpress.org/manzurahammed/
 * License:           GPLv2
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       tenup-post-grid
 * Domain Path:       /languages/
 */

/**
 * Defining plugin constants.
 *
 * @since 3.0.0
 */
define( 'TENUP_POST_GRID_PLUGIN_FILE', __FILE__ );
define( 'TENUP_POST_GRID_BASENAME', plugin_basename( __FILE__ ) );
define( 'TENUP_POST_GRID_PATH', trailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'TENUP_POST_GRID_URL', trailingslashit( plugins_url( '/', __FILE__ ) ) );
define( 'TENUP_POST_GRID_VERSION', '1.0.0' );

require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

class Tenup_Post_Grid {
    private static $instance = null;
    
    /**
     * Create singleton instance
     *
     * @return Tenup_Post_Grid|null
     */
    public static function get_instance() {
        if ( !self::$instance ) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    public function __construct() {
        $this->init();
        add_action( 'wp_loaded', [$this, 'version_update'] );
        register_activation_hook(
            __FILE__,
            [
                $this,
                'activate'
            ]
        );
        register_deactivation_hook(
            __FILE__,
            [$this, 'deactivate']
        );
    }
    
    /**
     * Init short code class
     */
    public function init() {
        new TenUpPostGrid\Enqueue;
        new TenUpPostGrid\Post_Grid_Builder;
    }
    
    /**
     * Update plugin version when new version arrive
     */
    public function version_update() {
        // set current version to db
        if ( get_option( 'tenup_post_grid_version' ) != TENUP_POST_GRID_VERSION ) {
            // update plugin version
            update_option( 'tenup_post_grid_version', TENUP_POST_GRID_VERSION );
        }
    }
    
    /**
     * Run this method when PLUGIN active
     */
    public function activate() {
       //todo
    }
    
    /**
     * Run this method when plugin deactive
     */
    public function deactivate() {
        //todo
    }
}

add_action( 'plugins_loaded', ['Tenup_Post_Grid', 'get_instance'] );

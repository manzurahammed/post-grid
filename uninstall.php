<?php
/**
 * Remove plugin data.
 *
 * @since 1.0
 * @package tenup_post_grid
 */

delete_option('tenup_post_grid_version');